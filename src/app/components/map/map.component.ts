import {AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';

declare var H: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {
  @ViewChild('map', {static: false})
  public mapElement: ElementRef;
  private map;
  private ui;

  @Input()
  public appId: any = 'IkVG1gMYxHv0fDCrrxDl';

  @Input()
  public appCode: any = 'G0EaPB8vFmovxk__QLZXXw';

  @Input()
  public lat: any = '50';

  @Input()
  public lng: any = '19';

  @Input()
  public width: any = '100vw';

  @Input()
  public height: any = '100%';

  public constructor() { }

  public ngOnInit() {

  }

  @HostListener('window:resize')
  onResize() {
    this.map.getViewPort().resize();
  }

  public ngAfterViewInit() {
    const platform = new H.service.Platform({
      app_id: this.appId,
      app_code: this.appCode
    });
    const defaultLayers = platform.createDefaultLayers();
    this.map = new H.Map(
      this.mapElement.nativeElement,
      defaultLayers.normal.map,
      {
        zoom: 5,
        center: { lat: this.lat, lng: this.lng }
      }
    );
    const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));
    this.ui = H.ui.UI.createDefault(this.map, defaultLayers);
    this.ui.getControl('mapsettings').setVisibility(false);

  }

}
