import { Component } from '@angular/core';
import {UserService} from './services/user.service';
import {Router} from '@angular/router';
import 'hammerjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pup';
  isLeftMenuOpened = false;
  isRightMenuOpened = false;
  constructor(private user: UserService, private router: Router) {}
  showHeading() {
    return this.user.checkIsUserLogged();
  }
  logout() {
    this.isLeftMenuOpened = false;
    this.isRightMenuOpened = false;
    this.user.logout();
    this.router.navigate(['login']);
  }
  toogleLeftMenu() {
    console.log('left click');
    this.isRightMenuOpened = false;
    this.isLeftMenuOpened = !this.isLeftMenuOpened;
  }
  toogleRightMenu() {
    console.log('right click');
    this.isLeftMenuOpened = false;
    this.isRightMenuOpened = !this.isRightMenuOpened;
  }
  closeMenu() {
    console.log('close menu');
    this.isLeftMenuOpened = false;
    this.isRightMenuOpened = false;
  }
  hammerjs() {
    console.log('hammer');
  }
  openMap() {
    this.router.navigate(['map']);
  }
}
