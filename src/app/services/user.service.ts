import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private isUserLogged = false;
  constructor() { }
  checkIsUserLogged() {
    return this.isUserLogged;
  }
  login() {
    this.isUserLogged = true;
  }
  logout() {
    this.isUserLogged = false;
  }
}
