import { TestBed, async, inject } from '@angular/core/testing';

import { AuthorizingGuard } from './authorizing.guard';

describe('AuthorizingGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizingGuard]
    });
  });

  it('should ...', inject([AuthorizingGuard], (guard: AuthorizingGuard) => {
    expect(guard).toBeTruthy();
  }));
});
